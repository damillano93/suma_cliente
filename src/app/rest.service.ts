import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

const endpoint = 'http://api.damillano.com:3000/operaciones/';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private http: HttpClient) {}




  addSuma (product): Observable<any> {
    console.log(product);
    return this.http.post<any>(endpoint + 'suma', JSON.stringify(product), httpOptions).pipe(
      tap(() => console.log(`ok`)),
      catchError(this.handleError<any>('addProduct'))
    );
  }

  addResta (product): Observable<any> {
    console.log(product);
    return this.http.post<any>(endpoint + 'resta', JSON.stringify(product), httpOptions).pipe(
      tap(() => console.log(`ok`)),
      catchError(this.handleError<any>('addProduct'))
    );
  }


  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
