import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';


import { ProductAddComponent } from './product-add/product-add.component';

const appRoutes: Routes = [
  {
    path: 'product-add',
    component: ProductAddComponent,
    data: { title: 'Suma' },
    pathMatch: 'full'
  },
  { path: '',
    redirectTo: '/product-add',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    ProductAddComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    FormsModule,
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
