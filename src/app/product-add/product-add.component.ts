import { Component, OnInit, Input } from '@angular/core';
import { RestService } from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.css']
})
export class ProductAddComponent implements OnInit {

  @Input() productData = { datoa: '', datob: '', resultado: 0 };
  @Input() Resultado = '';

  constructor(public rest: RestService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
  }
  addSuma() {
    this.rest.addSuma(this.productData).subscribe((result) => {
      console.log(result);
      this.Resultado = this.productData.datoa + '+'  + this.productData.datob + '= ' + result.resultado;
      this.limpiar();
    }, (err) => {
      console.log(err);
    });
  }
  addResta() {
    this.rest.addResta(this.productData).subscribe((result) => {
      console.log(result);
      this.Resultado = this.productData.datoa + '-'  + this.productData.datob + '= ' + result.resultado;
      this.limpiar ();
    }, (err) => {
      console.log(err);
    });
  }
  limpiar () {
    this.productData.datoa = '';
    this.productData.datob = '';
   }

}
