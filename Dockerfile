# Cuso de la imagen de nodejs
FROM node
# Copiar el archivo package.json
COPY package*.json ./
# Instalar modulos npm
RUN npm install
# Instalar angular CLI
RUN npm install -g @angular/cli
# Copiar los archivos de codigo fuente
COPY . .
# Seleccionar el directorio de trabajo
WORKDIR /suma_cliente
# ejecutar servicio
CMD ng serve --host 0.0.0.0